
import java.io.*;
import javax.servlet.http.*;


public class TempServlet extends HttpServlet{
    @Override
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Temperature converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"lightgrey\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("tempCelsius") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                new TempConv().getFahrenheit(request.getParameter("tempCelsius")) +
                "</BODY></HTML>");
    }

}
