import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TempConvTest {
    @Test
    public void conversionTest() {
        TempConv t = new TempConv();
        Assertions.assertEquals(64,t.getFahrenheit("32.0"));

    }
}
